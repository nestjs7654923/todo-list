<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

<p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
<p align="center">
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
  <a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
  <a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
  <a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
  <a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
  <a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
  <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>

## Description

This project is a NestJS-based application for managing Todo Lists. It provides a RESTful API for creating, updating, deleting, and retrieving todo lists and their items.

The project is structured based on Domain-Driven Design (DDD) principles and uses the SAGA pattern for managing complex business transactions and workflows. It also follows Clean Architecture principles and incorporates CQRS, Event-Driven, and Repository design patterns.

The application is modular, with separate modules for Todo, Auth, and User functionalities.

## Features

- User authentication and authorization
- CRUD operations for Todo Lists
- CRUD operations for Todo Items within a list
- Priority management for Todo Items
- E2E testing with Jest and Supertest

## Project Structure

The project follows Domain-Driven Design (DDD) principles and is organized into the following layers:

- **Domain**: Contains the core business logic and domain models.
- **Application**: Contains the application services and use cases.
- **Infrastructure**: Contains the implementation details, such as repositories and external services.
- **Interfaces**: Contains the controllers and API endpoints.

### Domain Layer

The domain layer contains the core business logic and domain models. It includes:

- **Entities**: Represent the core business objects.
- **Value Objects**: Represent immutable objects that describe some aspect of the domain.
- **Aggregates**: Group related entities and value objects.
- **Repositories**: Abstract the data access layer.

### Application Layer

The application layer contains the application services and use cases. It includes:

- **Services**: Implement the business logic and coordinate the domain objects.
- **DTOs**: Data Transfer Objects used for communication between layers.

### Infrastructure Layer

The infrastructure layer contains the implementation details, such as repositories and external services. It includes:

- **Repositories**: Implement the data access logic.
- **Database**: Configuration and setup for the database.

### Interfaces Layer

The interfaces layer contains the controllers and API endpoints. It includes:

- **Controllers**: Handle HTTP requests and responses.
- **DTOs**: Data Transfer Objects used for communication with the client.

### SAGA Pattern

The SAGA pattern is used to manage complex business transactions and workflows. It ensures data consistency across multiple services by coordinating a series of local transactions.


## Installation

To install the dependencies, run:
```
bash
$ yarn install
```
## Running the app

## Running the app with Docker Compose

To start the application with Docker Compose, follow these steps:

1. Ensure you have Docker and Docker Compose installed on your machine.
2. Create a `.env` file in the root of your project and add the necessary environment variables (e.g., database connection details).
3. Build and start the containers:

```
bash
cd deployment
$ docker-compose up --build
```

This will start the application and the mongo database. The application will be available at `http://localhost:3000`.

## Test

To run the tests, use the following commands:
#unit tests
```
bash
cd deployment
$  docker-compose -f docker-compose.test.yml up --abort-on-container-exit

```

#e2e tests
```
bash
$  ./run-e2e-tests.sh
```


## API Endpoints

### Authentication

- **POST /auth/register**: Register a new user
- **POST /auth/login**: Login and obtain a JWT token

### Todo Lists

- **GET /todo-lists**: Get all todo lists for the authenticated user
- **POST /todo-lists**: Create a new todo list
- **PUT /todo-lists/:id**: Update a todo list
- **PATCH /todo-lists/update-priority**: Update the priority of a todo item
- **DELETE /todo-lists/:id**: Delete a todo list

### Example Requests

#### Register a new user


```
bash
 curl -X POST http://localhost:3000/auth/register -H 'Content-Type: application/json' -d '{"username": "testuser", "password": "testpassword"}'
```


#### Login

```
bash 
 curl -X POST http://localhost:3000/auth/login -H 'Content-Type: application/json' -d '{"username": "testuser", "password": "testpassword"}'
```

#### Create a Todo List

```
bash 
curl -X POST http://localhost:3000/todo-lists -H 'Authorization: Bearer <your_token>' -H 'Content-Type: application/json' -d '{"title": "My Todo List"}'
```



## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [hessam zaheri](https://www.linkedin.com/in/hessam-zaheri)

## License

Nest is [MIT licensed](LICENSE).
