#!/bin/bash

# Define the path to the docker-compose file
DOCKER_COMPOSE_FILE=deployment/docker-compose.yml

# Start the Docker containers
docker-compose -f $DOCKER_COMPOSE_FILE up -d

# Wait for the application to be ready
echo "Waiting for the application to start..."
sleep 10

# Run the e2e tests
docker-compose -f $DOCKER_COMPOSE_FILE exec app npm run test:e2e

# Stop the Docker containers
docker-compose -f $DOCKER_COMPOSE_FILE down