import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { TodoListService } from '../src/modules/todo/application/services/todo-list.service';
import { CreateTodoListDto } from '../src/modules/todo/interfaces/dto/create-todo-list.dto';
import { TodoList } from '../src/modules/todo/domain/models/todo-list.model';
import { UpdateTodoItemPriorityDto } from '../src/modules/todo/interfaces/dto/update-todo-item-priority.dto';

describe('TodoListController (e2e)', () => {
  let app: INestApplication;
  let authToken: string;
  const todoListService = {
    getAllTodoLists: () => Promise.resolve([]),
    createTodoList: () =>
      Promise.resolve(new TodoList('user123', 'Test List', [])),
    updateTodoList: () =>
      Promise.resolve(new TodoList('user123', 'Updated List', [])),
    updateTodoItemPriority: () =>
      Promise.resolve(new TodoList('user123', 'Test List', [])),
    deleteTodoList: () =>
      Promise.resolve(new TodoList('user123', 'delete Test List', [])),
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(TodoListService)
      .useValue(todoListService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    // Register a new user
    await request(app.getHttpServer())
      .post('/auth/register')
      .send({ username: 'testuser', password: 'testpassword' });

    // Obtain authentication token
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'testuser', password: 'testpassword' });
    authToken = response.body.access_token; // Adjust according to your auth response structure
  });

  it('/todo-lists (GET)', () => {
    return request(app.getHttpServer())
      .get('/todo-lists')
      .set('Authorization', `Bearer ${authToken}`)
      .expect(200)
      .expect([]);
  });

  it('/todo-lists (POST)', () => {
    const createTodoListDto = new CreateTodoListDto();
    createTodoListDto.title = 'Test List';
    return request(app.getHttpServer())
      .post('/todo-lists')
      .set('Authorization', `Bearer ${authToken}`)
      .send(createTodoListDto)
      .expect(201)
      .expect({
        title: 'Test List',
        userId: 'user123',
        items: [],
      });
  });

  it('/todo-lists/:id (PUT)', () => {
    const createTodoListDto = new CreateTodoListDto();
    createTodoListDto.title = 'Updated List';
    return request(app.getHttpServer())
      .put('/todo-lists/todoListId')
      .set('Authorization', `Bearer ${authToken}`)
      .send(createTodoListDto)
      .expect(200)
      .expect({
        title: 'Updated List',
        userId: 'user123',
        items: [],
      });
  });

  it('/todo-lists/update-priority (PATCH)', () => {
    const updateTodoItemPriorityDto: UpdateTodoItemPriorityDto = {
      todoListId: 'todoListId',
      itemId: 'itemId',
      newPriority: 1,
    };
    return request(app.getHttpServer())
      .patch('/todo-lists/update-priority')
      .set('Authorization', `Bearer ${authToken}`)
      .send(updateTodoItemPriorityDto)
      .expect(200)
      .expect({
        title: 'Test List',
        userId: 'user123',
        items: [],
      });
  });

  it('/todo-lists/:id (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/todo-lists/todoListId')
      .set('Authorization', `Bearer ${authToken}`)
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
