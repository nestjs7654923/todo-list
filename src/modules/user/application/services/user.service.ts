import { Injectable } from '@nestjs/common';
import { User } from '../../domain/models/user.model';
import { UserRepository } from '../../infrastructure/repositories/user.repository.impl';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async register(user: User): Promise<User> {
    return this.userRepository.create(user);
  }

  async findByUsername(username: string): Promise<User> {
    return this.userRepository.findByUsername(username);
  }
}
