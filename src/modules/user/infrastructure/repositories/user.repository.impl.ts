import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDocument } from '../schemas/user.schema';
import { User } from '../../domain/models/user.model';
import { UserMapper } from '../../application/mappers/user.mapper';

@Injectable()
export class UserRepository {
  constructor(
    @InjectModel(UserDocument.name) private userModel: Model<UserDocument>,
  ) {}

  async create(user: User): Promise<User> {
    const userDocument = new this.userModel(
      UserMapper.toPersistence(user) as UserDocument,
    );
    const createdUser = await userDocument.save();
    return UserMapper.toDomain(createdUser as UserDocument);
  }

  async findByUsername(username: string): Promise<User> {
    const userDocument = await this.userModel.findOne({ username }).exec();
    return userDocument
      ? UserMapper.toDomain(userDocument as UserDocument)
      : null;
  }
}
