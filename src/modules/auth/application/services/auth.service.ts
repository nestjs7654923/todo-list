// src/modules/auth/application/services/auth.service.ts
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UserService } from '../../../user/application/services/user.service';
import { UserMapper } from '../../../user/application/mappers/user.mapper';
import { User } from '../../../user/domain/models/user.model';
import { CreateUserDto } from '../../../user/interfaces/dto/create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<User> {
    const user = await this.userService.findByUsername(username);
    if (user && (await bcrypt.compare(pass, user.password))) {
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async register(createUserDto: CreateUserDto): Promise<User> {
    const user = UserMapper.fromCreateUserDto(createUserDto);
    const hashedPassword = await bcrypt.hash(user.password, 10);
    const newUser = new User(
      user.id,
      user.username,
      hashedPassword,
     // user.todoLists,
    );
    return this.userService.register(newUser);
  }
}
