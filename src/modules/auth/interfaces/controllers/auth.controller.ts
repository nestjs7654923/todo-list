import { Controller, Post, Body, Request, UseGuards } from '@nestjs/common';
import { AuthService } from '../../application/services/auth.service';
import { LocalAuthGuard } from '../../infrastructure/guards/local-auth.guard';
import { CreateUserDto } from '../../../user/interfaces/dto/create-user.dto';
import { Public } from '../../infrastructure/decorators/public.decorator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Public()
  @Post('register')
  async register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }
}
