import { TodoList } from '../../domain/models/todo-list.model';

export interface ITodoListRepository {
  findAllLean(filter: any): Promise<TodoList[]>;
  create(todoList: TodoList): Promise<TodoList>;
  update(id: string, todoList: TodoList): Promise<TodoList>;
  updateTodoItemPriority(
    todoListId: string,
    itemId: string,
    newPriority: number,
  ): Promise<TodoList>;
  delete(id: string): Promise<void>;
  findByIdAndUserId(id: string, userId: string): Promise<TodoList | null>;
}
