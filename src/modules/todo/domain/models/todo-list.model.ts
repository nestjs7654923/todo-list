import { Types } from 'mongoose';
import { TodoItem } from './todo-item.model';

export class TodoList {

  userId: string;
  title: string;
  items: Types.DocumentArray<TodoItem>; // Ensure items is typed as DocumentArray

  constructor(userId: string, title: string, items: TodoItem[]) {
    this.userId = userId;
    this.title = title;
    this.items = items as Types.DocumentArray<TodoItem>; // Type assertion to DocumentArray
  }
}
