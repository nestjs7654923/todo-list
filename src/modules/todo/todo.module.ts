import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TodoListController } from './interfaces/controllers/todo-list.controller';
import { TodoListService } from './application/services/todo-list.service';
import {
  TodoListDocument,
  TodoListSchema,
} from './infrastructure/schemas/todo-list.schema';
import {
  TodoItemDocument,
  TodoItemSchema,
} from './infrastructure/schemas/todo-item.schema';
import { AuthModule } from '../auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '../auth/infrastructure/guards/jwt-auth.guard';
import { TodoListRepository } from './infrastructure/repositories/todo-item.repository.impl';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: TodoListDocument.name, schema: TodoListSchema },
      { name: TodoItemDocument.name, schema: TodoItemSchema },
    ]),
    AuthModule,
  ],
  controllers: [TodoListController],
  providers: [
    TodoListService,
    TodoListRepository,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class TodoModule {}
