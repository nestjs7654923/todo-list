import { CreateTodoListDto } from '../../interfaces/dto/create-todo-list.dto';
import { TodoList } from '../../domain/models/todo-list.model';
import { TodoItem } from '../../domain/models/todo-item.model';

export class TodoListMapper {
  static toDomain(
    createTodoListDto: CreateTodoListDto,
    userId: string,
  ): TodoList {
    const items = (createTodoListDto.todoItems || []) // Ensure todoItems is not undefined
      .map(
        (item) => new TodoItem(item.description, item.completed, item.priority),
      )
      .sort((a, b) => a.priority - b.priority); // Sort items by priority

    return new TodoList(userId, createTodoListDto.title, items);
  }
}
