import { Test, TestingModule } from '@nestjs/testing';
import { TodoListService } from '../todo-list.service';
import { TodoList } from '../../../domain/models/todo-list.model';
import { TodoListRepository } from '../../../infrastructure/repositories/todo-item.repository.impl';

describe('TodoListService', () => {
  let service: TodoListService;
  let repository: TodoListRepository;

  const mockTodoListRepository = {
    findAllLean: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    updateTodoItemPriority: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TodoListService,
        {
          provide: TodoListRepository,
          useValue: mockTodoListRepository,
        },
      ],
    }).compile();

    service = module.get<TodoListService>(TodoListService);
    repository = module.get<TodoListRepository>(TodoListRepository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllTodoLists', () => {
    it('should return an array of todo lists', async () => {
      const userId = 'user123';
      const result: TodoList[] = [];
      jest.spyOn(repository, 'findAllLean').mockResolvedValue(result);

      expect(await service.getAllTodoLists(userId)).toStrictEqual(result);
    });
  });

  // Add more tests for other service methods
});
