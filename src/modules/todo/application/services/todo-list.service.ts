import { Injectable } from '@nestjs/common';
import { TodoList } from '../../domain/models/todo-list.model';
import { TodoListRepository } from '../../infrastructure/repositories/todo-item.repository.impl';

@Injectable()
export class TodoListService {
  constructor(private readonly todoListRepository: TodoListRepository) {}

  async getAllTodoLists(userId: string): Promise<TodoList[]> {
    const todoLists = await this.todoListRepository.findAllLean({ userId });
    return todoLists.map((list) => ({
      ...list,
      items: list.items.sort((a, b) => a.priority - b.priority),
    }));
  }
  async createTodoList(todoList: TodoList) {
    return this.todoListRepository.create(todoList);
  }

  async updateTodoList(id: string, todoList: TodoList) {
    return this.todoListRepository.update(id, todoList);
  }

  async updateTodoItemPriority(
    todoListId: string,
    itemId: string,
    newPriority: number,
  ): Promise<TodoList> {
    return this.todoListRepository.updateTodoItemPriority(
      todoListId,
      itemId,
      newPriority,
    );
  }

  async deleteTodoList(id: string, userId: string): Promise<void> {
    const todoList = await this.todoListRepository.findByIdAndUserId(
      id,
      userId,
    );
    if (!todoList) {
      throw new Error(
        'Todo list not found or you do not have permission to delete it',
      );
    }
    await this.todoListRepository.delete(id);
  }
}
