import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { isValidObjectId, Model } from 'mongoose';
import { TodoList } from '../../domain/models/todo-list.model';
import { TodoListDocument } from '../schemas/todo-list.schema';
import { TodoItemDocument } from '../schemas/todo-item.schema';
import { TodoItem } from '../../domain/models/todo-item.model';
import { ITodoListRepository } from '../../domain/repositories/todo-list.repository';

@Injectable()
export class TodoListRepository implements ITodoListRepository {
  constructor(
    @InjectModel(TodoListDocument.name)
    private todoListModel: Model<TodoListDocument>,
    @InjectModel(TodoItemDocument.name)
    private todoItemModel: Model<TodoItemDocument>,
  ) {}

  private toDomain(todoListDocument: TodoListDocument): TodoList {
    const { userId, title, items } = todoListDocument;
    return new TodoList(userId, title, items as TodoItem[]);
  }

  async findAllLean(filter: any): Promise<TodoList[]> {
    const result = await this.todoListModel
      .find(filter)
      .populate('items')
      .lean()
      .exec();
    return result.map(this.toDomain) as TodoList[]; // Transform each document to domain model
  }

  async create(todoList: TodoList): Promise<TodoList> {
    const createdTodoList = new this.todoListModel(todoList);
    console.log('Before save:', JSON.stringify(createdTodoList, null, 2));
    await createdTodoList.save();
    console.log('After save:', JSON.stringify(createdTodoList, null, 2));
    return this.toDomain(
      createdTodoList.toObject({ versionKey: false }) as TodoListDocument,
    );
  }

  async update(id: string, todoList: TodoList): Promise<TodoList> {
    if (!isValidObjectId(id)) {
      throw new BadRequestException('Invalid ID format');
    }

    const existingTodoList = await this.todoListModel.findById(id).exec();
    if (!existingTodoList) {
      throw new NotFoundException('TodoList not found');
    }

    const updatedTodoList = await this.todoListModel
      .findByIdAndUpdate(id, todoList, { new: true })
      .exec();
    return this.toDomain(updatedTodoList.toObject() as TodoListDocument);
  }

  async updateTodoItemPriority(
    todoListId: string,
    itemId: string,
    newPriority: number,
  ): Promise<TodoList> {
    const todoList = await this.todoListModel.findById(todoListId).exec();
    if (!todoList) {
      throw new NotFoundException('TodoList not found');
    }

    const item = todoList.items.id(itemId); // Ensure items is accessed here
    if (!item) {
      throw new NotFoundException('TodoItem not found');
    }

    item.priority = newPriority;
    await todoList.save();
    return this.toDomain(todoList.toObject() as TodoListDocument);
  }

  async delete(id: string): Promise<void> {
    if (!isValidObjectId(id)) {
      throw new BadRequestException('Invalid ID format');
    }

    const todoList = await this.todoListModel.findById(id).exec();
    if (!todoList) {
      throw new NotFoundException('TodoList not found');
    }

    // Delete all associated TodoItems
    await this.todoItemModel
      .deleteMany({ _id: { $in: todoList.items } })
      .exec();

    // Delete the TodoList
    await this.todoListModel.findByIdAndDelete(id).exec();
  }
  async findByIdAndUserId(
    id: string,
    userId: string,
  ): Promise<TodoList | null> {
    if (!isValidObjectId(id)) {
      throw new BadRequestException('Invalid ID format');
    }

    const todoListDocument = await this.todoListModel
      .findOne({ _id: id, userId })
      .exec();
    return todoListDocument
      ? this.toDomain(todoListDocument.toObject() as TodoListDocument)
      : null;
  }
}
