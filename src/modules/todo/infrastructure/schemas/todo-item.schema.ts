import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema({ collection: 'todoItems' })
export class TodoItemDocument extends Document {

  @Prop({ required: true })
  description: string;

  @Prop({ default: false })
  completed: boolean;

  @Prop({ required: true, default: 1 })
  priority: number;
}

export const TodoItemSchema = SchemaFactory.createForClass(TodoItemDocument);

