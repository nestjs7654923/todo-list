import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { TodoItemDocument, TodoItemSchema } from './todo-item.schema'; // Ensure this import is correct

@Schema()
export class TodoListDocument extends Document {
  @Prop({ required: true })
  title: string;

  @Prop({ type: [TodoItemSchema], default: [] })
  items: Types.DocumentArray<TodoItemDocument>;

  @Prop({ required: true })
  userId: string;
}

export const TodoListSchema = SchemaFactory.createForClass(TodoListDocument);
