import {
  IsString,
  IsArray,
  ValidateNested,
  IsNotEmpty,
  IsOptional,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateTodoItemDto } from './create-todo-item.dto';

export class CreateTodoListDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateTodoItemDto)
  @IsOptional()
  todoItems: CreateTodoItemDto[];
}
