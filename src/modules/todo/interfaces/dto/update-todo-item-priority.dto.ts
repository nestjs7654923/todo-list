import { IsNotEmpty } from 'class-validator';

export class UpdateTodoItemPriorityDto {
  @IsNotEmpty()
  todoListId: string;
  @IsNotEmpty()
  itemId: string;
  @IsNotEmpty()
  newPriority: number;
}
