import { IsString, IsBoolean, IsOptional } from 'class-validator';

export class CreateTodoItemDto {
  @IsString()
  description: string;

  @IsBoolean()
  @IsOptional()
  completed?: boolean;

  @IsOptional()
  priority?: number;
}
