import {
  Controller,
  Post,
  Put,
  Body,
  Param,
  Get,
  Patch,
  UseGuards, Delete,
} from '@nestjs/common';
import { TodoListService } from '../../application/services/todo-list.service';
import { CreateTodoListDto } from '../dto/create-todo-list.dto';
import { TodoListMapper } from '../../application/mappers/todo-list.mapper';
import { TodoList } from '../../domain/models/todo-list.model';
import { UpdateTodoItemPriorityDto } from '../dto/update-todo-item-priority.dto';
import { User } from '../../../auth/infrastructure/decorators/user.decorator';

@Controller('todo-lists')
export class TodoListController {
  constructor(private readonly todoListService: TodoListService) {}

  @Get()
  async getAllTodoLists(@User() user: any): Promise<TodoList[]> {
    const userId = user.userId;
    return this.todoListService.getAllTodoLists(userId);
  }

  @Post()
  async createTodoList(
    @Body() createTodoListDto: CreateTodoListDto,
    @User() user: any,
  ) {
    const userId = user.userId;
    const todoList = TodoListMapper.toDomain(createTodoListDto, userId);
    return this.todoListService.createTodoList(todoList);
  }

  @Put(':id')
  async updateTodoList(
    @Param('id') id: string,
    @Body() createTodoListDto: CreateTodoListDto,
    @User() user: any,
  ) {
    const userId = user.userId;
    const todoList = TodoListMapper.toDomain(createTodoListDto, userId);
    return this.todoListService.updateTodoList(id, todoList);
  }

  @Patch('update-priority')
  async updateTodoItemPriority(
    @Body() updateTodoItemPriorityDto: UpdateTodoItemPriorityDto,
  ): Promise<TodoList> {
    const { todoListId, itemId, newPriority } = updateTodoItemPriorityDto;
    return this.todoListService.updateTodoItemPriority(
      todoListId,
      itemId,
      newPriority,
    );
  }

  @Delete(':id')
  async deleteTodoList(@Param('id') id: string, @User() user: any) {
    const userId = user.userId;
    return this.todoListService.deleteTodoList(id, userId);
  }
}
