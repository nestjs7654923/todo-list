import { Test, TestingModule } from '@nestjs/testing';
import { TodoListController } from '../todo-list.controller';
import { TodoListService } from '../../../application/services/todo-list.service';
import { CreateTodoListDto } from '../../dto/create-todo-list.dto';
import { TodoList } from '../../../domain/models/todo-list.model';
import { UpdateTodoItemPriorityDto } from '../../dto/update-todo-item-priority.dto';

describe('TodoListController', () => {
  let controller: TodoListController;
  let service: TodoListService;

  const mockTodoListService = {
    getAllTodoLists: jest.fn(),
    createTodoList: jest.fn(),
    updateTodoList: jest.fn(),
    updateTodoItemPriority: jest.fn(),
    deleteTodoList: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TodoListController],
      providers: [
        {
          provide: TodoListService,
          useValue: mockTodoListService,
        },
      ],
    }).compile();

    controller = module.get<TodoListController>(TodoListController);
    service = module.get<TodoListService>(TodoListService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllTodoLists', () => {
    it('should return an array of todo lists', async () => {
      const userId = 'user123';
      const result: TodoList[] = [];
      jest.spyOn(service, 'getAllTodoLists').mockResolvedValue(result);

      expect(await controller.getAllTodoLists({ userId })).toBe(result);
    });
  });

  describe('createTodoList', () => {
    it('should create a new todo list', async () => {
      const userId = 'user123';
      const createTodoListDto = new CreateTodoListDto();
      createTodoListDto.title = 'Test List';
      const result: TodoList = new TodoList(userId, 'Test List', []);

      jest.spyOn(service, 'createTodoList').mockResolvedValue(result);

      expect(
        await controller.createTodoList(createTodoListDto, { userId }),
      ).toBe(result);
    });
  });

  describe('updateTodoList', () => {
    it('should update a todo list', async () => {
      const userId = 'user123';
      const id = 'todoListId';
      const createTodoListDto = new CreateTodoListDto();
      createTodoListDto.title = 'Updated List';
      const result: TodoList = new TodoList(userId, 'Updated List', []);

      jest.spyOn(service, 'updateTodoList').mockResolvedValue(result);

      expect(
        await controller.updateTodoList(id, createTodoListDto, { userId }),
      ).toBe(result);
    });
  });

  describe('updateTodoItemPriority', () => {
    it('should update the priority of a todo item', async () => {
      const updateTodoItemPriorityDto: UpdateTodoItemPriorityDto = {
        todoListId: 'todoListId',
        itemId: 'itemId',
        newPriority: 1,
      };
      const result: TodoList = new TodoList('user123', 'Test List', []);

      jest.spyOn(service, 'updateTodoItemPriority').mockResolvedValue(result);

      expect(
        await controller.updateTodoItemPriority(updateTodoItemPriorityDto),
      ).toBe(result);
    });
  });

  describe('deleteTodoList', () => {
    it('should delete a todo list', async () => {
      const userId = 'user123';
      const id = 'todoListId';

      jest.spyOn(service, 'deleteTodoList').mockResolvedValue(undefined);

      await controller.deleteTodoList(id, { userId });

      expect(service.deleteTodoList).toHaveBeenCalledWith(id, userId);
    });
  });
});
